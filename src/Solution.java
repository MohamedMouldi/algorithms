import java.util.Arrays;

/**
 * @author M.Slama
 */
public class Solution {

    /**
     * Odd number
     * @param A
     * @return
     */
    public int firstAlgorithm( int [] A){
        Arrays.sort(A);
        int len = A.length;
        if (len % 2 == 0){
            throw new IllegalArgumentException();
        }
        if (len == 1){
            return A[0];
        }
        for (int i = 0; i < len; i+=2 ){
            if (i+1 == len || A[i]!= A[i+1]){
                return A[i];
            }
        }
        return -1;
    }

    /**
     * Max Counters
     *
     * @param N
     * @param A
     * @return
     */
    public int[] secondAlgorithm(int N, int[] A) {

        int currentMax = 0;
        int lastUpdate = 0;
        int countersArray[] = new int[N];

        for (int i = 0; i < A.length; i++) {
            if (A[i] == N + 1) {
                lastUpdate = currentMax;
            } else {
                int position = A[i] - 1;
                if (countersArray[position] < lastUpdate)
                    countersArray[position] = lastUpdate + 1;
                else
                    countersArray[position]++;

                if (countersArray[position] > currentMax) {
                    currentMax = countersArray[position];
                }
            }

        }

        for (int i = 0; i < N; i++)
            if (countersArray[i] < lastUpdate)
                countersArray[i] = lastUpdate;

        return countersArray;
    }

    /**
     *
     * Climb the ladder
     * @param A
     * @param B
     * @return
     */
    public int[] thirdAlgorithm(int[] A, int[] B){
        int max = getMax(A);
        int [] listFibonacci = fib(max);
        int [] result = new int[A.length];

        for (int i = 0; i<A.length; i++) {
            result[i] = (int) (listFibonacci[A[i]+1] % Math.pow(2, B[i]));
        }

        return result;
    }

    private int getMax(int[] array) {
        int max = array[0];

        for (int i = 0; i<array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        return max;
    }

    private int[] fib(int n) {
        int [] A = new int[n+2];
        for (int i = 0; i<=n+1; i++) {
            if ((i == 0) || (i == 1)){
                A[i] = i;
            } else {
                A[i]= A[i-1] + A[i-2];
            }
        }
        return A;
    }

}
